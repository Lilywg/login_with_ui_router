var express = require("express");
var app = express();

var path = require("path");

var session = require("express-session");

var bodyParser = require("body-parser");

var ensure = require("connect-ensure-login");
var passport = require("passport");
var PassportLocal = require("passport-local");
var local = new PassportLocal(
    { usernameField: "username", passwordField: "password" },
    function(username, password, done) {
        if (username == password)
            return (done(null, username));
        return (done(null, false));
    });

passport.use(local);
passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(id, done) {
    done(null, {username: id});
});

app.use(session({
    secret: "hello",
    resave: false,
    saveUninitialized: false
}));

app.use(bodyParser.urlencoded({extended: false}));

app.use(passport.initialize());
app.use(passport.session());

app.use("/protected/*", ensure.ensureLoggedIn("/status/401"));

app.post("/authenticate", passport.authenticate("local", {
    successRedirect: "/status/200",
    failureReturnToOrRedirect: "/status/403"
}));

app.get("/status/:code", function(req, res) {
    //res.status(parseInt(req.params.code)).send();
    res.sendStatus(parseInt(req.params.code));
});

app.get("/logout", function(req, res) {
    req.logout();
    req.session.destroy();
    res.sendStatus(200);
})

app.use("/bower_components", express.static(path.join(__dirname, "bower_components")));
app.use(express.static(path.join(__dirname, "public")));

app.set("port", process.env.APP_PORT || 3000);

app.listen(app.get("port"), function() {
    console.info("Application started on port %d", app.get("port"));
})